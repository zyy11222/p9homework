import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);


import axios from "./utils/request"
Vue.prototype.axios = axios;

router.beforeEach((to, from, next) => {
  console.log(to.path);

  let token = localStorage.getItem("token");

  if (to.path != "/login") {
    if (token == null) {
      next("/login")
    }
  }
  next();
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
