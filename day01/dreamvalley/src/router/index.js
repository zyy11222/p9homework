import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../components/home'),
    children: [
      {
        path: '/home-page',
        name: 'HomePage',
        component: () => import('../components/homePage')
      },
      {
        path: '/management',
        name: 'Management',
        component: () => import('../components/Management')
      },
      {
        path: '/supplier',
        name: 'Supplier',
        component: () => import('../components/Supplier')
      },
      {
        path: '/commodity',
        name: 'Commodity',
        component: () => import('../components/Commodity')
      },
      {
        path: '/employee',
        name: 'Employee',
        component: () => import('../components/Employee')
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../components/login')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
