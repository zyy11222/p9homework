import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/login';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/',
    name: 'Leyout',
    component: () => import('../views/Leyout')
  },
  {
    path: '/home',
    name: 'Leyout',
    component: () => import('../views/Leyout'),
    redirect: '/home-page',
    children: [
      {
        path: '/home-page',
        name: 'HomePage',
        component: () => import('../components/homePage'),
      },
      {
        path: '/management',
        name: 'Management',
        meta:{title:'会员管理'},
        component: () => import('../components/Management'),
      },
      {
        path: '/supplier',
        name: 'Supplier',
        meta:{title:'供应商管理'},
        component: () => import('../components/Supplier'),
        title:'/supplier'
      },
      {
        path: '/commodity',
        name: 'Commodity',
        meta:{title:'商品管理'},
        component: () => import('../components/Commodity'),
        title:'/commodity'
      },
      {
        path: '/employee',
        name: 'Employee',
        meta:{title:'员工管理'},
        component: () => import('../components/Employee'),
        title:'/employee'
      }
    ]
  },
];

const router = new VueRouter({
  routes,
});

export default router;
