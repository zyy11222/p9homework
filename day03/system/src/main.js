import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import "./assets/css/reset.css"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  console.log(to.path);

  let token = sessionStorage.getItem("token");

  if (to.path != "/login") {
    if (token == null) {
      next("/login")
    }
  }
  next();
})

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
