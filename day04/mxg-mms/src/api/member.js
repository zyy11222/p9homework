/**
 * 
 * 会员相关的api接口: 会员列表 、 新增会员 、 删除会员、 修改会员
 * 
 */

import request from "../utils/request"

//会员列表
let getMemberList = (page=1,size=10,data)=>{
    return request({
        url : `/member/list/${page}/${size}`,
        method : "GET",
        params : data
    })
}

//删除会员
let deleteMember = (id) => {
    return request({
        url : `/member/${id}`, 
        method : "DELETE"
    })
}

export default {
    getMemberList,
    deleteMember
}