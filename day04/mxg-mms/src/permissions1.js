// 当用户登录以后,后台会返回一个token,我们会把这个token保存到本地和vuex里面,



// 有token的时候说明用户已经登录了

// 没有token的时候说明用户没有登录



// 页面鉴权: 在页面跳转到的时候,在路由守卫里面判断vuex或者本地有没有token



// 没有token ---> 用户是没有登录的 ---> 

// ​								不能进入到登录以后的页面

// ​								用户没有登录的时候能不能进入到登录页  ---> 能进入


//引入router对象
import router from "./router"
//引入store对象
import store from "./store"

//定义路由守卫方法
router.beforeEach((to,form,next)=>{
    //获取vuex里面的token
    let token = store.state.token;
    //获取vuex里面的用户信息userInfo
    let userInfo = store.state.userInfo;

    //判断token是否存在  -->存在已登录   -->不存在 未登录
    if(!token){
        //判断用户在没有登录的情况下是否访问登录页 -> 是 - 进入 
        if(to.path == "/login"){
            //进入
            next()
        }else{ 
            //进入的不是登录页
            next("/login")
        }
    }else{
        //判断进入到是不是登录页
        if(to.path == "/login"){
            next("/login")
        }else{
            //进入的是登录以后的页面,判断是否有用户信息
            /**
             * 有用户信息
             *      next()
             * 没有用户信息
             *      调用获取用户信息方法, 我们在vuex里面已经定义了获取用户信息方法,所以我们在这个直接调用vuex里面定义的获取用户信息方法
             */
            if(userInfo){ 
                //有用户信息,直接可以进入到要进入的页面
                next()
            }else{
                //没有用户信息
                store.dispatch("getUserInfo").then(response=>{
                    if(response.data.flag){
                        //获取用户信息成功,可以进入到登录以后的页面
                        next()
                    }else{
                        //获取用户信息失败,跳转到登录页,重新登录
                        next("/login")
                    }   
                }).catch(error=>{
                    console.log(error)
                })
            }
        }
    }
})

