/**
 * 
 * 员工管理API接口: 员工列表、查询、新增、编辑
 * 
 */
import request from "../utils/request"

//员工列表接口
let getStaffList = (page,size,data) => {
    return request({
        url : `/staff/list/${page}/${size}`,
        method : "GET",
        params : data
    })
};

//员工删除接口
let deleteStaff = (id)=>{
    return request({
        url : `/staff/${id}`,
        method : "DELETE"
    })
}

//员工单条查询接口
let findStaff = (id)=>{
    return request({
        url : `/staff/${id}`,
        method : "GET"
    })
}

//员工新增接口
let addStaff = (data)=>{
    return request({
        url : "/staff",
        method : "POST",
        data
    })
}

//员工编辑接口
let editStaff = (id,data)=>{
    return request({
        url : `/staff/${id}`,
        method : "PUT",
        data
    })
}

export default {
    getStaffList,
    deleteStaff,
    findStaff,
    addStaff,
    editStaff
}