/**
 * 
 * 目标: 
 *  当用户没有登录的时候,不允许用户访问登录以后的页面
 * 
 *  当用户登录成功后,可以访问登录以后的页面
 * 
 * 
 *      拆解需求:
 *            没有token
 * 
 *              用户进入的是不是登录页
 * 
 *                  是登录页 
 *                          进入
 *                  
 *                  不是登录页
 *                          进入到登录页
 *                      
 * 
 *            有token
 *                  
 *              用户进入的是不是登录页
 *          
 *                              是
 *                                      进入或者不进入
 * 
 *                              不是
 *                                      判断有没有用户信息
 * 
 *                                                有用户信息
 *                                                      进入
 *                                                 没有
 * 、                                                   调用获取用户信息接口
 *                                                          成功了
 *                                                              进入
 *                                                          不成功
 *                                                              进入登录页
 *          
 * 
 */
//引入路由对象
import router from "./router"
//引入store
import store from "./store"

router.beforeEach((to,form,next)=>{
    //获取存储在本地或者vuex里面的token
    let token = store.state.token;
    let userInfo = store.state.userInfo;

    //判断token是否存在,如果存在则表示用户已经登录, 如果不存在,则表示用户没有登录
    if(!token){
        if(to.path == "/login"){
            next()
        }else{
            next("/login")
        }
    }else{
        if(to.path == "/login"){
            next()
        }else{
            if(!userInfo){
                store.dispatch("getUserInfo").then(response=>{
                    if(response.data.flag){
                        next()
                    }else{
                        next("/login")
                    }
                }).catch(error=>{
                    console.log(error)
                })
            }else{
                next()
            }
        }
    }
})
