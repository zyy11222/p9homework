import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/login';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path : "/",
    name : "layout",
    redirect : "/home",
    component: () => import('../components/Layout.vue'),
    children : [
      {
        path : "/home",
        name : "home",
        component: () => import('../views/home'),
      }
    ]
  },
  {
    path : "/member",
    component : () => import('../components/Layout.vue'),
    children : [
      {
        path : "/",
        meta : {title : "会员管理"},
        component : () => import('../views/member')
      }
    ]
  }, 
  {
    path : "/supplier",
    component : () => import('../components/Layout.vue'),
    children : [
      {
        path : "/",
        meta : {title : "供应商管理"},
        component : () => import('../views/supplier')
      }
    ]
  },
  {
    path : "/goods",
    component : () => import('../components/Layout.vue'),
    children : [
      {
        path : "/",
        meta : {title : "商品管理"},
        component : () => import('../views/goods')
      }
    ]
  },
  {
    path : "/staff",
    component : () => import('../components/Layout.vue'),
    children : [
      {
        path : "/",
        meta : {title : "员工管理"},
        component : () => import('../views/staff')
      }
    ]
  },
  {
    path : "*",
    redirect : "/home"
  }
];

const router = new VueRouter({
  routes,
});

export default router;
