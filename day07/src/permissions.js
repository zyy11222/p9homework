// 引入router对象
import router from "./router/index"
// 引入store对象
import store from "./store/index"

// 分两种情况
/**
 * 一种是有token值，
 * 一种是没有token的值
 * 
 * 
 * ** */

router.beforeEach((to, from, next) => {
    //先去获取vuex里面的token
    let token = store.state.token
        // 获取vuex里面的信息userInfo
    let userInfo = store.state.userInfo
        // 接下来判断token是否存在
        // 如果存在的话就是登陆了 
    if (!token) {
        // 判断用户在未登录的情况下访问登录页面
        if (to.path == "/login") {
            // 如果访问的是登录页面,就让他进入
            next()
        } else {
            // 如果访问的不是登录页面
            // 就强制让他进入登录页面
            next('/login')
        }
    } else {
        // 存在token的时候
        // 判断进入的是不是登录页面
        if (to.path == '/login') {
            next('/login')
        } else {
            // 如果进入的是登陆以后的页面，判断他是否有用户信息，
            // 如果有的话， 登录以后的页面随便进，如果没有获取用户信息
            if (userInfo) {
                next()
            } else {
                store.dispatch('userInfo').then(response => {
                    if (response.data.flag) {
                        // 获取信息成功随便进
                        next()
                    } else {
                        // 获取信息失败
                        // 进入登录页面
                        next('/login')
                    }
                }).catch(error => {
                    console.log(error);
                })
            }
        }
    }
})