import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/login';
Vue.use(VueRouter);

const routes = [{
        path: '/login',
        name: 'login',
        component: Login,
    },
    {
        path: '/',
        name: 'Loyout',
        component: () =>
            import ('../components/Loyout.vue'),
        redirect: "/home",
        children: [{
                path: '/home',
                name: 'home',
                component: () =>
                    import ('../views/home/index.vue'),
            }, {
                path: '/menber',
                name: 'menber',
                meta: { title: "会员管理" },
                component: () =>
                    import ('../views/menber/index.vue'),
            },
            {
                path: '/supplier',
                name: 'supplier',
                meta: { title: "供应商管理" },
                component: () =>
                    import ('../views/supplier/index.vue'),
            },
            {
                path: '/goods',
                name: 'goods',
                meta: { title: "商品管理" },
                component: () =>
                    import ('../views/goods/index.vue'),
            },
            {
                path: '/staff',
                name: 'staff',
                meta: { title: "员工管理" },
                component: () =>
                    import ('../views/staff/index.vue'),
            },
        ]
    },
    {
        path: "*",
        redirect: "/home"
    }
];

const router = new VueRouter({
    routes,
});

export default router;