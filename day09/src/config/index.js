// // 环境变量
// 本地开发环境
// 测试环境
// 预发布环境
// 生产环境（线上环境）

// 获取当前运行的环境  开发环境、测试环境、生产环境
const env = process.env.NODE_ENV || "production"

// 定义不同情况下请求的代理名称和接口地址
const EnvConfig = {
    // 开发环境
    development: {
        baseApi: "/dev-api",
        mockApi: "https://www.fastmock.site/mock/76ef040b1066810a6a9e8b7cf636e63d/oa",
    },
    // 测试环境
    test: {
        baseApi: "/test-api",
        mockApi: "https://www.fastmock.site/mock/76ef040b1066810a6a9e8b7cf636e63d/oa",
    },
    // 生产环境
    production: {
        baseApi: "pro-api",
        mockApi: "https://oa.9yuecloud.com"
    },

}
export default {
    env,
    mock: true,
    ...EnvConfig['development']
}