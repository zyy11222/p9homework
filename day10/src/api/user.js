// 用户相关的接口

// 引入axios请求文件
import request from "../utils/request"

export default {
    // 用户登录的接口
    login(data) {
        return request({
            url: "/users/login",
            method: "POST",
            data
        })
    }
}