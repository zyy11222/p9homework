import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// element ui 组件库
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.config.productionTip = false;

// 引入storage
import storage from "./utils/storage"

// 将storage进行全局注册
Vue.prototype.$storage = storage

//引⼊userApi
import userApi from "./api/user"

//将userApi挂载到全局
Vue.prototype.userApi = userApi;

Vue.use(ElementUI);

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');