/*utils/storage*/
/***
 * Storage⼆次封装 
 */
//1. 在config/index.js⽂件内创建存储的命名空间 "namespace" : "manage",在storage.js⽂件
// 内引⼊ config / index.js
import config from "../config/index.js"
//2. 封装storage      
export default {
    setItem(key, val) {
        let storage = this.getStorage();
        storage[key] = val;
        window.localStorage.setItem(config.namespace, JSON.stringify(storage));
    },
    getItem(key) {
        return this.getStorage()[key];
    },
    getStorage() {
        return JSON.parse(window.localStorage.getItem(config.namespace) || "{}");
    },
    clearItem(key) {
        let storage = this.getStorage();
        delete storage[key];
        window.localStorage.setItem(config.namespace, JSON.stringify(storage));
    },
    clearAll() {
        window.localStorage.clear()
    }
}