//引入axios请求文件
import request from "../utils/request"

//导出封装的api接口
export default {
    //登录接口
    login(data) {
        return request({
            url: "/users/login",
            method: "POST",
            data
        })
    },
    //获取菜单权限接口
    getPermissionList() {
        return request({
            url: "/users/getPermissionList",
            method: "GET"
        })
    }
}