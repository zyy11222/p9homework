//引入request
import request from "../utils/request"

//封装菜单接口并导出
export default {
    //菜单列表接口
    getMenuList(data){
        return request({
            url : "/menu/list",
            method : "GET",
            data
        })
    },
    //菜单 新增/编辑/删除 接口
    createMenu(data){
        return request({
            url : "/menu/operate",
            method : "POST",
            data
        })
    }
}