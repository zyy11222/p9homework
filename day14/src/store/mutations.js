import storage from '../utils/storage'; 
export default {
    //保存用户信息
    saveUserInfo(state,userInfo){
        state.userInfo = userInfo;
        storage.setItem("userInfo",userInfo)
    },
    //保存菜单列表
    saveMenuList(state,menuList){
        state.menuList = menuList;
        storage.setItem("menuList",menuList)
    },
    //保存按钮权限列表
    saveActionList(state,actionList){
        state.actionList = actionList;
        storage.setItem("actionList",actionList)
    },
}