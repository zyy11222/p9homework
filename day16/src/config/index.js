/**
 * 
 * 环境变量: 
 *          本地开发环境        development                 http://dev.9yuecloud.com
 *          测试环境                                       http://test.9yuecloud.com 
 *          生产环境(线上环境)    production                http://www.9yuecloud.com
 * 
 * 
 *   在不同的开发模式下请求不同的接口地址
 * 
*/

import axios from "axios";

//获取当前运行的环境  开发环境/测试环境/生产环境 development
const env = process.env.NODE_ENV || "production";

//定义不同环境下请求的代理名称和接口地址
const EnvConfig = {
    //开发环境
    development : {
        //真实的开发阶段的后台api
        baseApi : "/dev-api",
        //模拟数据的接口地址
        mockApi : "https://www.fastmock.site/mock/c1c302e8baed9894c48c17e4738c092e/api"
    }
}

// baseApi 开发阶段真实的公共接口地址

/**
 * 
 *  proxy : {
 *      "/dev-api" : {
 *          target : "http://oa.9yuecloud.com:3333/api"
 *       }
 * }
 * 
*/

// mockApi 开发阶段mock数据的接口地址 (什么时候用mockApi接口地址? 真实接口还没有完成开发的时候)

//mock是定义的一个变量
//如果mock的值 为true , 我就让请求的公共接口地址为mockApi
//如果mock的值 为false, 我就让请求的公共接口地址为baseApi

// axios里面如何设置默认的公共接口地址

// let url = "http://oa.9yuecloud.com:3333/api"

//两种:

// 
// 2. axios.create({baseURL : "http://oa.9yuecloud.com:3333/api"})

// 1. axios.defaults.baseURL = "http://oa.9yuecloud.com:4444/api"

// axios.get("/users/login")
// axios.get("/users/menu")
// axios.get("/menus/menu")

// let mock = true;

// if(mock == true){
//     axios.defaults.baseURL = mockApi
// }else{
//     axios.defaults.baseURL = baseApi
// }

// axios.defaults.baseURL = mock ? mockApi : baseApi



export default {
    env,
    mock : false,
    namespace : "manager",
    ...EnvConfig["development"]
   
}





























//  //测试环境
//  test : {
//     baseApi : "/test-api",
//     mockApi : "https://test.fastmock.site/mock/76ef040b1066810a6a9e8b7cf636e63d/oa"
// },
// //生产环境
// production : {
//     baseApi : "pro-api",
//     mockApi : "https://oa.9yuecloud.com"
// } 