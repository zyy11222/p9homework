import Vue from 'vue';
import ElementUI from 'element-ui';
import App from './App.vue';
import router from './router';
import store from './store';

// 引入element-ui相关
import 'element-ui/lib/theme-chalk/index.css';

// 注册element-ui的
Vue.use(ElementUI);

Vue.config.productionTip = false;

//引入storage
import storage from "./utils/storage"

//将storage进行全局注册
Vue.prototype.$storage = storage

//引入userApi
import userApi from "./api/user"
//引入menuApi
import menuApi from "./api/menu"
//引入roleApi
import roleApi from './api/role'
//引入deptList
import deptApi from "./api/dept"

//将userApi挂载到全局
Vue.prototype.userApi = userApi;
//将menuApi挂载到全局
Vue.prototype.menuApi = menuApi;
//将roleApi挂载到全局
Vue.prototype.roleApi = roleApi;
//将deptApi挂载到全局
Vue.prototype.deptApi = deptApi;

//检测当前的环境
console.log(process.env)

//初始化一个变量10
// let num = 10;

/**
 * 
 * 老和尚和小和尚讲故事 讲 5遍, 想让老和尚第一遍故事讲完之后,自动去讲第二遍
 * 
 * 计算1 - 5 和
 * 
*/
// let i = 0;
// function test(){
//   i = i + 1;
//   console.log(i)

//   console.log("老和尚给小和尚讲故事")
//   if(i == 5){
//     return false;
//   }
//   test()
// }
// test()

// function result(num){
//   if(num >= 1){
//     return num + result(num - 1)
//   }
//   return 0
// }

// console.log(result(5)) // 5 + 4 + 3 + 2 + 1  == 15

//日期格式化时间
import moment from "moment"
//将日期格式化时间挂载到全局
Vue.prototype.moment = moment;

/**
 * 
 *  1. 前端后的接口联调
 * 
 *    1.1 把mock的接口地址切换到真实的接口地址
 *    1.2 检查请求的接口地址以及传递的参数是否正确
 * 
 *    真实的接口地址和mock的接口地址不同的是什么?
 * 
 *    不同的是公共的接口地址
 *      
 *    "http://oa.9yuecloud.com:3333/api/users/login"
 *      
 *    http://oa.9yuecloud.com:3333/api  公共的接口地址
 * 
 *    /users/login  当前功能的接口地址
 * 
 *    前后端分离的情况下: 前端 可以不依赖后端进行并行开发      前端可以模拟数据   ---   接口文档
 *    
 *  2. 实现404页面的布局
 * 
 *    
 *  3. 当访问不存在的路由地址进入到404页面 (路由守卫)
 * 
 *  // 1. 获取到要进入的页面路由
 * 
 *    // 2. 获取到所有的路由地址
 * 
 *    // 3. 判断我要进入的页面路由地址 在 所有的路由地址里面是否存在
 * 
 *    // 4. 如果存在, 则进入
 * 
 *    // 5. 如果不存在 , 咱进入到404
 * 
 * 
 *  4. 动态路由
 * 
 * 
 */ 

import "./permissions"

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
