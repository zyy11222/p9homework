import Vue from 'vue';
import VueRouter from 'vue-router';
import store from "../store"
import utils from "../utils/utils"
import Login from "../views/Login.vue"
import userApi from "../api/user"

Vue.use(VueRouter);

const routes = [
  //登录路由
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/test",
    name: "test",
    component: () => import("../views/Test.vue")
  },
  {
    path: "/404",
    name: "404",
    component: () => import("../views/404.vue")
  },
  //主页路由
  {
    path: "/",
    name: "home",
    meta: { title: "首页" },
    component: () => import("../components/Home.vue"),
    redirect: "/welcome",
    children: [
      {
        path: "/welcome",
        name: "welcome",
        meta: { title: "欢迎体验Vue3全栈课程" },
        component: () => import("../views/Welcome.vue")
      }
    ]
  }
];

const router = new VueRouter({
  routes,
});

function _import(view) {
  return () => import("../views/" + view + ".vue")
}

//动态加载路由
async function loadAsyncRoute() {
    let {menuList} =await userApi.getPermissionList();
    //3. 将menuList的数据处理成一维数组
    let route = utils.generatorRoutes(menuList)
    //4. 将组件的路由结构动态的添加到路由表里面
    route.forEach(item => {
      item.component = _import(item.component)
      router.addRoute("home", item)
    })

    function checkPermission(path){
   
      //1. 要进入的路由已经获取到了 , path
  
      //2. 获取到所有的路由
      let allRoutes = router.getRoutes()
  
      //3. 过滤进入路由是否在所有路由中存在
      let hasPermission = allRoutes.filter((item,index)=> {
          return item.path == path
      })
  
  
      if(hasPermission.length > 0){
          return true
      }else{
          return false;
      }
  } 
}


loadAsyncRoute()

export {loadAsyncRoute}

export default router;















/***
 *
 *  动态路由?  路由表不是在前台写死的 , 而是通过后台返回给我们的
 *
 *  router.addRoutes()  可以将路由表动态的添加到路由里面
 *
 *  后台管理当中: 我们的路由表一般分为 静态路由和动态路由    router.addRoute()
 *
 *
 * // 问题: 路由文件里面如何获取getPermissionslist接口的数据  (菜单的数据)
 *          - 请求一下getPermissionslist接口
 *
 *          - 登录的时候有没有将getPermissionslist接口的数据保存到vuex
 *
 *
 *
 * 获取到菜单数据
 *
 * 将菜单的数据转化为路由所需要的结构
 *
 * 使用router.addRoute将路由的数据动态添加到路由表
 *
*/

// {
      //   path : "/system/user",
      //   name : "user",
      //   meta : {title : "用户管理"},
      //   component : ()=> import("../views/User.vue")
      // },
      // {
      //   path : "/system/menu",
      //   meta : {title : "菜单管理"},
      //   name : "menu",
      //   component : ()=> import("../views/Menu.vue")
      // },
      // {
      //   path : "/system/role",
      //   name : "role",
      //   meta : {title : "角色管理"},
      //   component : ()=> import("../views/Role.vue")
      // },
      // {
      //   path : "/system/dept",
      //   name : "dept",
      //   meta : {title : "部门管理"},
      //   component : ()=> import("../views/Dept.vue")
      // },
      // {
      //   path : "/audit/leave",
      //   name : "leave",
      //   meta : {title : "休假申请"},
      //   component : ()=> import("../views/Leave.vue")
      // },
      // {
      //   path : "/audit/approve",
      //   name : "approve",
      //   meta : {title : "待我审批"},
      //   component : ()=> import("../views/Approve.vue")
      // }